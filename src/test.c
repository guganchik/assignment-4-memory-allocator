#define _DEFAULT_SOURCE

#include "test.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>

struct block_header* block;

static int block_count(struct block_header* block){
    int count =0;
    struct block_header *iteration =block;
    while(iteration->next){
        if(!iteration->is_free){
            count+=1;
            iteration =iteration->next;
        }
    }
    return true;
}

static void delete_heap(void *heap,size_t sz){
    munmap(heap,size_from_capacity((block_capacity){.bytes=sz}).bytes);
}

void allocation_test() {
    printf("Test 1. Allocation_test \n");
    void* heap = heap_init(TEST_1_2_3_4);
    if(heap==NULL){
        fprintf(stderr, "Failed to initialize the heap\n");
        return;
    } printf("heap was init\n");
    debug_heap(stdout, heap);

    void* mem = _malloc(2048);
    if(mem==NULL){
        fprintf(stderr, "Failed to allocate memory\n");
        return;
    } printf("mem was alloc\n");
    debug_heap(stdout, heap);

    _free(mem);
    delete_heap(heap,TEST_1_2_3_4);
    printf("Succesful!(Test_1)\n");
}

void free_one_block_test() {
    printf("Test 2. Free_one_block_test\n");
    void* heap = heap_init(TEST_1_2_3_4);
    if(heap==NULL){
        fprintf(stderr, "Failed to initialize the heap\n");
        return;
    } printf("heap was init\n");
    debug_heap(stdout, heap);

    void* mem_1 = _malloc(256);
    if(mem_1==NULL){
        fprintf(stderr, "Failed to allocate memory\n");
        return;
    } printf("mem_1 was alloc\n");
    debug_heap(stdout, heap);

    void* mem_2 = _malloc(512);
    if(mem_2==NULL){
        fprintf(stderr, "Failed to allocate memory\n");
        return;
    } printf("mem_2 was alloc\n");
    debug_heap(stdout, heap);

    if(block_count(block)!=2){
        fprintf(stderr, "Not alloc two blocks!\n");
        fprintf(stderr, "Test_2 failed\n");
        return;
    }
    _free(mem_2);
    printf("mem_2 free\n");
    debug_heap(stdout, heap);

    if (block_count(block)==1){
        fprintf(stderr, "Test_2 failed\n");
        _free(mem_1);
        return;
    }
    printf("mem_1 free\n");
    _free(mem_1);
    delete_heap(heap,TEST_1_2_3_4);
    printf("Succesful!(Test_2)\n");
}

void free_two_blocks_test(){
    printf("Test 3. Free_two_blocks_test\n");
    void* heap = heap_init(TEST_1_2_3_4);
    if(heap==NULL){
        fprintf(stderr, "Failed to initialize the heap\n");
        return;
    } printf("heap was init\n");
    debug_heap(stdout, heap);

    void* mem_1 = _malloc(256);
    void* mem_2 = _malloc(512);
    void* mem_3 = _malloc(256);

    if(!mem_1||!mem_2||!mem_3){
        fprintf(stderr,"Memory is not allocated\n");
        return;
    }
    printf("Memory is allocated\n");


    debug_heap(stdout, heap);
    _free(mem_2);
    printf("Free mem_2\n");
    debug_heap(stdout, heap);
    _free(mem_3);
    printf("Free mem_3\n");
    debug_heap(stdout, heap);
    if(!mem_1){
        fprintf(stderr, "Test_3 failed!");
        return;
    }
    _free(mem_1);
    printf("Free mem_1\n");
    delete_heap(heap,TEST_1_2_3_4);
    printf("Succesful!(Test_3)\n");
}
void test_4(){
    printf("Test 4");
    void* heap = heap_init(TEST_1_2_3_4);
    void* mem_1 = _malloc(8176);
    debug_heap(stdout, heap);
    void* mem_2 = _malloc(256);
    debug_heap(stdout, heap);
    if(!mem_1){
        fprintf(stderr, "Failed alloc mem_1. Test_4 failed!");
        return;
    }
    if(!mem_2){
        fprintf(stderr, "Failed alloc mem_2. Test_4 failed!");
        return;
    }
    _free(mem_1);
    _free(mem_2);
    delete_heap(heap,TEST_1_2_3_4);
    printf("Succesful!(Test_4)\n");
}

void test_5(){
    printf("Test 5");
    void* heap = heap_init(TEST_5);
    void* mem_1 = _malloc(3000);
    debug_heap(stdout, heap);

     struct block_header* test_header = heap;
     (void)mmap(test_header->contents + test_header->capacity.bytes, 4800, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);

    void* mem_2 = _malloc(1000);
    debug_heap(stdout, heap);

    _free(mem_1);
    _free(mem_2);
    delete_heap(heap,TEST_5);
    printf("Succesful!(Test_5)\n");
}

void run_all_tests(){
    printf("~~~~~TESTS~~~~~~\n");

    allocation_test();

    free_one_block_test();

    free_two_blocks_test();

    test_4();

    test_5();

    printf("~~~~~TESTS~~~~~~\n");

}
