#ifndef _TEST_H_
#define _TEST_H_

#include "mem.h"
#include "mem_internals.h"

void allocation_test();
void free_one_block_test();
void free_two_blocks_test();
void test_4();
void test_5();

void run_all_tests();

enum{
    TEST_1_2_3_4=4096,
    TEST_5=3000
};

#endif
